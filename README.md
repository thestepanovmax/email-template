# Email template

## Tested in the next mail clients: 

### Desktop browsers: 
* Gmail;
* Mail.ru;
* Yandex.ru;

### Desktop clients: 
* Thunderbird v.52;
* Outlook 2016;

### Mobile clients:
* Android Gmail;
* iOS Gmail;
